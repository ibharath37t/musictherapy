package com.example.hp.musictherapy.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import com.example.hp.musictherapy.fragments.PlayListFragment;
import com.example.hp.musictherapy.fragments.TrackListFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {


    int tabCount;

    public PagerAdapter(FragmentManager fm, int tabCount) {

        super(fm);
        this.tabCount = tabCount;

    }


    @Override
    public Fragment getItem(int position) {

        switch (position) {

            case 0:
                return new TrackListFragment();

            case 1:
                return new PlayListFragment();

            default:
                return null;

        }

    }


    @Override
    public int getCount() {
        return tabCount;
    }

}