package com.example.hp.musictherapy.beans;

public class ArtistBean {

    private String artistid, artist;

    public ArtistBean(){

    }

    public ArtistBean(String artistid, String artist) {
        this.artistid = artistid;
        this.artist = artist;
    }

    public String getId() {
        return artistid;
    }

    public void setId(String id) {
        this.artistid = id;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

}
