package com.example.hp.musictherapy.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.example.hp.musictherapy.R;
import com.example.hp.musictherapy.adapters.ArtistAdapter;
import com.example.hp.musictherapy.adapters.DiseaseAdapter;
import com.example.hp.musictherapy.adapters.GenreAdapter;
import com.example.hp.musictherapy.beans.ArtistBean;
import com.example.hp.musictherapy.beans.DiseaseBean;
import com.example.hp.musictherapy.beans.GenreBean;
import com.example.hp.musictherapy.utils.Cache;
import com.example.hp.musictherapy.utils.DatabaseHelper;
import com.example.hp.musictherapy.utils.ServiceHandler;
import com.example.hp.musictherapy.utils.SessionManager;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;

public class CategoryActivity extends AppCompatActivity {


    Spinner selDisease, selGenre, selArtist, selRaga;
    Button okButton;
    ImageView logoutImage, backImage;
    SessionManager sessionManager;
    AlertDialog.Builder builder;
    ProgressDialog progressDialog;
    String dis_response = "", dis_url = "";
    String genre_response = "", genre_url = "";
    String artist_response = "", artist_url = "";
    ArrayList<DiseaseBean> diseaseBeans = new ArrayList<DiseaseBean>();
    ArrayList<GenreBean> genreBeans = new ArrayList<GenreBean>();
    ArrayList<ArtistBean> artistBeans = new ArrayList<ArtistBean>();
    String disid = "", disease = "", CacheDisease = "";
    String genid = "", genre = "";
    String artid = "", artist = "";
    DatabaseHelper helper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);


        selDisease = findViewById(R.id.diseaseSpinner);
        selGenre = findViewById(R.id.genreSpinner);
        selArtist = findViewById(R.id.artistSpinner);
        selRaga = findViewById(R.id.ragaSpinner);

        okButton = findViewById(R.id.okButton);
        logoutImage = findViewById(R.id.toolimage);
        backImage = findViewById(R.id.backimage);
        backImage.setVisibility(View.INVISIBLE);

        helper = new DatabaseHelper(CategoryActivity.this);
        sessionManager = new SessionManager(this);

        dis_url = getString(R.string.diseaseurl);
        genre_url = getString(R.string.genreurl);
        artist_url = getString(R.string.artisturl);


        final ConnectivityManager manager =
                (ConnectivityManager) getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);

        if (manager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                manager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                manager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                manager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

            new GetDisease().execute();
            new GetGenres().execute();
            //new GetArtist().execute();

        } else {

            Toast.makeText(this, "No Internet, please try again!", Toast.LENGTH_SHORT).show();
            finish();

        }


        selDisease.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {

                TextView dis = view.findViewById(R.id.diseaseText);
                disease = dis.getText().toString();
                TextView did = view.findViewById(R.id.diseaseId);
                disid = did.getText().toString();

                SharedPreferences preferences = getSharedPreferences("disease_pref", MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                //editor.putString("disid", disid);
                editor.putString("disease", disease);
                editor.apply();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });


        selGenre.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {

                TextView dis = view.findViewById(R.id.genreText);
                genre = dis.getText().toString();
                if (genre.equals("--Select--")){
                    genre = "";
                }
                TextView did = view.findViewById(R.id.genreId);
                genid = did.getText().toString();

                SharedPreferences preferences = getSharedPreferences("genre_pref", MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                //editor.putString("genid", genid);
                editor.putString("genre", genre);
                editor.apply();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });


        selArtist.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long l) {

                TextView dis = view.findViewById(R.id.artistText);
                artist = dis.getText().toString();
                TextView did = view.findViewById(R.id.artistId);
                artid = did.getText().toString();

                SharedPreferences preferences = getSharedPreferences("artist_pref", MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("artid", artid);
                //editor.putString("artist", artist);
                editor.apply();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }

        });


        builder = new AlertDialog.Builder(CategoryActivity.this);
        builder.setMessage("Do you want to Logout?")
                .setCancelable(false)
                .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        sessionManager.logoutUser();
                        helper.deleteFromAllTables();

                    }
                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();

                    }
                });


        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (manager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                        manager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                        manager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                        manager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

                    Intent intent = new Intent(CategoryActivity.this, TrackListActivity.class);
                    startActivity(intent);
                    finish();

                } else {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "No Internet, please try again!", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.MAGENTA);
                    mySnackbar.show();

                }


            }
        });


        logoutImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog alert = builder.create();
                alert.setTitle("Logout");
                alert.show();

            }
        });


    }


    @Override
    public void onBackPressed() {

        finish();
        super.onBackPressed();

    }


    public class GetDisease extends AsyncTask<Void, Void, Void> {

        ServiceHandler serviceHandler = new ServiceHandler();

        @Override
        protected void onPreExecute() {

            progressDialog = new ProgressDialog(CategoryActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                dis_response = serviceHandler.makeServiceCall(dis_url, ServiceHandler.GET);

            } catch (Exception e) {

                e.printStackTrace();
                dis_response = "Fail";

            }

            JSONObject jobject = null;

            try {

                if (Cache.getInstance().getLru().get(CacheDisease) != null) {

                    jobject = (JSONObject) Cache.getInstance().getLru().get(CacheDisease);

                } else {

                    jobject = new JSONObject(dis_response);

                    if (jobject != null) {

                        Cache.getInstance().getLru().put(CacheDisease, jobject);

                    }

                }

                for (int i = 0; i < jobject.getJSONArray("details").length(); i++) {

                    JSONObject jsonObject = jobject.getJSONArray("details").getJSONObject(i);
                    DiseaseBean bean = new DiseaseBean();
                    disid = jsonObject.getString("Diseases_Id");
                    disease = jsonObject.getString("Diseases");
                    bean.setId(disid);
                    bean.setDisease(disease);
                    diseaseBeans.add(bean);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            progressDialog.dismiss();
            selDisease.setAdapter(new DiseaseAdapter(getApplicationContext(), diseaseBeans));
            super.onPostExecute(aVoid);

        }

    }


    public class GetGenres extends AsyncTask<Void, Void, Void> {

        ServiceHandler serviceHandler = new ServiceHandler();

        @Override
        protected void onPreExecute() {

            /*progressDialog = new ProgressDialog(CategoryActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();*/

            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                genre_response = serviceHandler.makeServiceCall(genre_url, ServiceHandler.POST);

            } catch (Exception e) {

                e.printStackTrace();
                genre_response = "Fail";

            }

            JSONObject jobject1 = null;

            try {

                GenreBean bean1 = new GenreBean("", "--Select--");
                genreBeans.add(0, bean1);

                jobject1 = new JSONObject(genre_response);

                for (int i = 0; i < jobject1.getJSONArray("details").length(); i++) {

                    JSONObject jsonObject1 = jobject1.getJSONArray("details").getJSONObject(i);
                    GenreBean bean = new GenreBean();
                    genid = jsonObject1.getString("Genres_Id");
                    genre = jsonObject1.getString("Genres");
                    bean.setId(genid);
                    bean.setGenre(genre);
                    genreBeans.add(bean);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            //progressDialog.dismiss();
            selGenre.setAdapter(new GenreAdapter(getApplicationContext(), genreBeans));
            super.onPostExecute(aVoid);

        }

    }


    public class GetArtist extends AsyncTask<Void, Void, Void> {

        ServiceHandler serviceHandler = new ServiceHandler();

        @Override
        protected void onPreExecute() {

            /*progressDialog = new ProgressDialog(CategoryActivity.this);
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();*/

            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            try {

                artist_response = serviceHandler.makeServiceCall(artist_url, ServiceHandler.GET);

            } catch (Exception e) {

                e.printStackTrace();
                artist_response = "Fail";

            }

            JSONObject jobject2 = null;

            try {

                ArtistBean bean1 = new ArtistBean("", "--Select--");
                artistBeans.add(0, bean1);

                jobject2 = new JSONObject(artist_response);

                for (int i = 0; i < jobject2.getJSONArray("details").length(); i++) {

                    JSONObject jsonObject2 = jobject2.getJSONArray("details").getJSONObject(i);
                    ArtistBean bean = new ArtistBean();
                    artid = jsonObject2.getString("Artist_Id");
                    artist = jsonObject2.getString("Artist");
                    bean.setId(artid);
                    bean.setArtist(artist);
                    artistBeans.add(bean);

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            //progressDialog.dismiss();
            selArtist.setAdapter(new ArtistAdapter(getApplicationContext(), artistBeans));
            super.onPostExecute(aVoid);

        }

    }


}
