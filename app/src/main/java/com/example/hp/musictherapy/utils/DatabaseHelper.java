package com.example.hp.musictherapy.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.example.hp.musictherapy.beans.ArtistBean;
import com.example.hp.musictherapy.beans.DiseaseBean;
import com.example.hp.musictherapy.beans.GenreBean;
import com.example.jean.jcplayer.JcAudio;
import java.util.ArrayList;

public class DatabaseHelper extends SQLiteOpenHelper {


    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "musictherapy_db";


    // Create table SQL query
    public static final String TABLE_DISEASE = "diseaseTable";
    public static final String DISEASE_ID = "id";
    public static final String COLUMN_DISEASE = "disease";

    public static final String CREATE_DISEASE =
            "CREATE TABLE " + TABLE_DISEASE + "("
                    + DISEASE_ID + " INTEGER PRIMARY KEY, "
                    + COLUMN_DISEASE + " TEXT "
                    + ")";


    private static final String TABLE_GENRE = "genreTable";
    private static final String GENRE_ID = "id";
    private static final String COLUMN_GENRE = "genre";

    public static final String CREATE_GENRE =
            "CREATE TABLE " + TABLE_GENRE + "("
                    + GENRE_ID + " INTEGER PRIMARY KEY, "
                    + COLUMN_GENRE + " TEXT "
                    + ")";


    public static final String TABLE_ARTIST = "artistTable";
    public static final String ARTIST_ID = "id";
    public static final String COLUMN_ARTIST = "artist";

    public static final String CREATE_ARTIST =
            "CREATE TABLE " + TABLE_ARTIST + "("
                    + ARTIST_ID + " INTEGER PRIMARY KEY, "
                    + COLUMN_ARTIST + " TEXT "
                    + ")";


    public static final String TABLE_TRACK = "trackTable";
    public static final String TRACK_ID = "id";
    public static final String COLUMN_TRACKNAME = "trackname";
    public static final String COLUMN_TRACKURL = "trackurl";

    public static final String CREATE_TRACK =
            "CREATE TABLE " + TABLE_TRACK + "("
                    + TRACK_ID + " INTEGER PRIMARY KEY, "
                    + COLUMN_TRACKNAME + " TEXT, "
                    + COLUMN_TRACKURL + " TEXT "
                    + ")";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create notes table
        db.execSQL(CREATE_DISEASE);
        db.execSQL(CREATE_GENRE);
        db.execSQL(CREATE_ARTIST);
        db.execSQL(CREATE_TRACK);

    }


    public void deleteFromAllTables() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_DISEASE);
        db.execSQL("DELETE FROM " + TABLE_GENRE);
        db.execSQL("DELETE FROM " + TABLE_ARTIST);
        db.execSQL("DELETE FROM " + TABLE_TRACK);
        db.close();

    }


    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DISEASE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GENRE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRACK);
        // Create tables again
        onCreate(db);

    }


    public long insertTrack(int tId, String tName, String tUrl) {


        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(TRACK_ID, tId);
        values.put(COLUMN_TRACKNAME, tName);
        values.put(COLUMN_TRACKURL, tUrl);

        // insert row
        long id = db.insert(TABLE_TRACK, null, values);
        // close db connection
        db.close();
        // return newly inserted row id
        return id;

    }


    public boolean checkTrack(String tname) {

        SQLiteDatabase db = getWritableDatabase();
        String selectString = "SELECT * FROM " + TABLE_TRACK + " WHERE " + COLUMN_TRACKNAME + " =?";
        Cursor cursor = db.rawQuery(selectString, new String[]{tname});

        boolean hasObject = false;

        if (cursor.moveToFirst()) {

            hasObject = true;

            int count = 0;
            while (cursor.moveToNext()) {
                count++;
            }

        }

        cursor.close();          // Dont forget to close your cursor
        db.close();              //AND your Database!
        return hasObject;
    }


    public ArrayList<JcAudio> getTracks() {
        ArrayList<JcAudio> jcAudios = new ArrayList<JcAudio>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_TRACK;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                JcAudio bean = new JcAudio();
                bean.setId(Long.parseLong(cursor.getString(cursor.getColumnIndex(TRACK_ID))));
                bean.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_TRACKNAME)));
                bean.setPath(cursor.getString(cursor.getColumnIndex(COLUMN_TRACKURL)));
                jcAudios.add(bean);
            } while (cursor.moveToNext());

        }

        // close db connection
        db.close();
        // return notes list
        return jcAudios;

    }


    public void deleteTrack(String tname) {

        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_TRACK, COLUMN_TRACKNAME + " = ?",
                new String[] { tname });
        db.close();

    }


    public void deleteAllTracks() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_TRACK);
        db.close();

    }


    public long insertDisease(ArrayList<DiseaseBean> diseaseBeans) {

        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(DISEASE_ID, diseaseBeans.get(0).getId());
        values.put(COLUMN_DISEASE, diseaseBeans.get(1).getDisease());

        // insert row
        long id = db.insert(TABLE_DISEASE, null, values);
        // close db connection
        db.close();
        // return newly inserted row id
        return id;

    }


    public long insertGenre(ArrayList<GenreBean> genreBeans) {

        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(GENRE_ID, genreBeans.get(0).getId());
        values.put(COLUMN_GENRE, genreBeans.get(1).getGenre());

        // insert row
        long id = db.insert(TABLE_GENRE, null, values);
        // close db connection
        db.close();
        // return newly inserted row id
        return id;

    }


    public long insertArtist(ArrayList<ArtistBean> artistBeans) {

        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ARTIST_ID, artistBeans.get(0).getId());
        values.put(COLUMN_ARTIST, artistBeans.get(1).getArtist());

        // insert row
        long id = db.insert(TABLE_ARTIST, null, values);
        // close db connection
        db.close();
        // return newly inserted row id
        return id;

    }


    public ArrayList<DiseaseBean> getDisease() {
        ArrayList<DiseaseBean> diseaseBeans = new ArrayList<DiseaseBean>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_DISEASE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                DiseaseBean bean = new DiseaseBean();
                bean.setId(cursor.getString(cursor.getColumnIndex(DISEASE_ID)));
                bean.setDisease(cursor.getString(cursor.getColumnIndex(COLUMN_DISEASE)));
                diseaseBeans.add(bean);
            } while (cursor.moveToNext());

        }

        // close db connection
        db.close();
        // return notes list
        return diseaseBeans;

    }


    public ArrayList<GenreBean> getGenre() {
        ArrayList<GenreBean> genreBeans = new ArrayList<GenreBean>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_GENRE;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                GenreBean bean = new GenreBean();
                bean.setId(cursor.getString(cursor.getColumnIndex(GENRE_ID)));
                bean.setGenre(cursor.getString(cursor.getColumnIndex(COLUMN_GENRE)));
                genreBeans.add(bean);
            } while (cursor.moveToNext());

        }

        // close db connection
        db.close();
        // return notes list
        return genreBeans;

    }


    public ArrayList<ArtistBean> getArtist() {
        ArrayList<ArtistBean> genreBeans = new ArrayList<ArtistBean>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_ARTIST;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {

            do {
                ArtistBean bean = new ArtistBean();
                bean.setId(cursor.getString(cursor.getColumnIndex(ARTIST_ID)));
                bean.setArtist(cursor.getString(cursor.getColumnIndex(COLUMN_ARTIST)));
                genreBeans.add(bean);
            } while (cursor.moveToNext());

        }

        // close db connection
        db.close();
        // return notes list
        return genreBeans;

    }


}
