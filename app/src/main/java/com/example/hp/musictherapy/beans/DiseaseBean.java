package com.example.hp.musictherapy.beans;

public class DiseaseBean {

    private String diseaseid, disease;

    public DiseaseBean(){

    }

    public DiseaseBean(String diseaseid, String disease) {
        this.diseaseid = diseaseid;
        this.disease = disease;
    }

    public String getId() {
        return diseaseid;
    }

    public void setId(String id) {
        this.diseaseid = id;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

}
