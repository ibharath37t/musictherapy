package com.example.hp.musictherapy.utils;

import android.graphics.Bitmap;
import android.util.LruCache;

public class Cache {

        private static Cache instance;
        private LruCache<Object, Object> lru;
        private  LruCache<String,Bitmap> bitMapCache;
        private Cache() {

            lru = new LruCache<Object, Object>(1024);
            bitMapCache = new LruCache<String,Bitmap>(1024);
        }

        public static Cache getInstance() {


            if (instance == null) {

                instance = new Cache();
            }

            return instance;

        }

        public LruCache<Object, Object> getLru() {
            return lru;
        }

        public LruCache<String, Bitmap> getLruBitmap() {
            return bitMapCache;
        }

}
