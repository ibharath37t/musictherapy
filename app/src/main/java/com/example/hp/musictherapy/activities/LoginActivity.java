package com.example.hp.musictherapy.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import com.example.hp.musictherapy.R;
import com.example.hp.musictherapy.utils.ServiceHandler;
import com.example.hp.musictherapy.utils.SessionManager;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {


    EditText nametext, mobtext;
    TextInputLayout nameT, mobT;
    Button getStartBtn;
    ImageView toolbarImage, backImage;
    String name = "", mobile = "", userid = "";
    String login_response = "", login_url = "";
    ProgressDialog progressDialog;
    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        nametext = findViewById(R.id.nametext);
        mobtext = findViewById(R.id.mobiletext);
        nameT = findViewById(R.id.usernameWrapper);
        mobT = findViewById(R.id.mobnumWrapper);
        getStartBtn = findViewById(R.id.getStartBtn);
        toolbarImage = findViewById(R.id.toolimage);
        backImage = findViewById(R.id.backimage);

        toolbarImage.setVisibility(View.INVISIBLE);
        backImage.setVisibility(View.INVISIBLE);

        sessionManager = new SessionManager(getApplicationContext());


        getStartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                login_url = getString(R.string.loginurl);

                name = nametext.getText().toString();
                mobile = mobtext.getText().toString();
                String mobPat = "^[0-9]{10}";

                if (name.isEmpty() || name.equals("")) {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Please enter name", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.MAGENTA);
                    mySnackbar.show();

                } else if (mobile.isEmpty() || mobile.equals("")) {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Please enter mobile number", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.MAGENTA);
                    mySnackbar.show();

                } else if (mobile.length() < 10) {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Please enter valid mobile number", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.MAGENTA);
                    mySnackbar.show();

                } else if (mobile.length() > 10) {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Please enter valid mobile number", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.MAGENTA);
                    mySnackbar.show();

                } else if (!mobile.matches(mobPat)) {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Please enter valid mobile number", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.MAGENTA);
                    mySnackbar.show();

                } else {

                    ConnectivityManager manager =
                            (ConnectivityManager) getSystemService(getApplicationContext().CONNECTIVITY_SERVICE);

                    if (manager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTED ||
                            manager.getNetworkInfo(0).getState() == android.net.NetworkInfo.State.CONNECTING ||
                            manager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTING ||
                            manager.getNetworkInfo(1).getState() == android.net.NetworkInfo.State.CONNECTED) {

                        new LoginValidation().execute();

                    } else {

                        Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                                "No Internet, please try again!", Snackbar.LENGTH_SHORT);
                        View sbView = mySnackbar.getView();
                        sbView.setBackgroundColor(Color.MAGENTA);
                        mySnackbar.show();

                    }


                }


            }
        });

    }


    public class LoginValidation extends AsyncTask<Void, Void, Void> {

        ServiceHandler serviceHandler = new ServiceHandler();


        @Override
        protected void onPreExecute() {

            userid = "";
            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Verifying please wait");
            progressDialog.setCancelable(false);
            progressDialog.show();

            super.onPreExecute();

        }


        @Override
        protected Void doInBackground(Void... voids) {

            List<NameValuePair> valuePairs = new ArrayList<>(2);
            valuePairs.add(new BasicNameValuePair("UserName", name));
            valuePairs.add(new BasicNameValuePair("Mobile", mobile));

            try {

                login_response = serviceHandler.makeServiceCall(login_url, serviceHandler.POST, valuePairs);

            } catch (Exception e) {
                e.printStackTrace();
                login_response = "Fail";
            }

            JSONObject jsonObject = null;
            try {

                jsonObject = new JSONObject(login_response);

                if (jsonObject.length() != 0) {

                    userid = jsonObject.getJSONArray("detail").getJSONObject(0).getString("User_Info_Id");
                    name = jsonObject.getJSONArray("detail").getJSONObject(0).getString("username");
                    mobile = jsonObject.getJSONArray("detail").getJSONObject(0).getString("mobile");

                } else {

                    Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                            "Wrong Details", Snackbar.LENGTH_SHORT);
                    View sbView = mySnackbar.getView();
                    sbView.setBackgroundColor(Color.MAGENTA);
                    mySnackbar.show();

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {

            if (userid.equals("")) {

                progressDialog.dismiss();
                Snackbar mySnackbar = Snackbar.make(findViewById(R.id.linearLayout),
                        "please try again!", Snackbar.LENGTH_SHORT);
                View sbView = mySnackbar.getView();
                sbView.setBackgroundColor(Color.MAGENTA);
                mySnackbar.show();

            } else {

                progressDialog.dismiss();
                sessionManager.createLoginSession(userid, name, mobile);
                finish();
                Intent intent = new Intent(getApplicationContext(), CategoryActivity.class);
                startActivity(intent);

            }

            super.onPostExecute(aVoid);
        }


    }


}
