package com.example.hp.musictherapy.beans;

public class GenreBean {

    private String genreid, genre;

    public GenreBean() {

    }

    public GenreBean(String genreid, String genre) {
        this.genreid = genreid;
        this.genre = genre;
    }

    public String getId() {
        return genreid;
    }

    public void setId(String Id) {
        this.genreid = Id;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

}
