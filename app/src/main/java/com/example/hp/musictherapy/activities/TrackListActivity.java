package com.example.hp.musictherapy.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.example.hp.musictherapy.R;
import com.example.hp.musictherapy.adapters.PagerAdapter;
import com.example.hp.musictherapy.utils.DatabaseHelper;
import com.example.hp.musictherapy.utils.SessionManager;

public class TrackListActivity extends AppCompatActivity
        implements TabLayout.OnTabSelectedListener {


    ImageView logoutImage, backImage;
    TextView tooltext;
    SessionManager sessionManager;
    AlertDialog.Builder builder;
    TabLayout tabLayout;
    ViewPager viewPager;
    PagerAdapter adapter;
    DatabaseHelper helper;
    String disease = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_list);

        helper = new DatabaseHelper(TrackListActivity.this);

        builder = new AlertDialog.Builder(TrackListActivity.this);
        builder.setMessage("Do you want to Logout?")
                .setCancelable(false)
                .setPositiveButton("Logout", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        sessionManager.logoutUser();
                        helper.deleteFromAllTables();

                    }
                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();

                    }
                });


        logoutImage = findViewById(R.id.toolimage);
        backImage = findViewById(R.id.backimage);
        tooltext = findViewById(R.id.tooltext);
        sessionManager = new SessionManager(this);

        SharedPreferences preferences = getSharedPreferences("disease_pref", MODE_PRIVATE);
        //diseaseid = preferences.getString("disid", "");
        disease = preferences.getString("disease", "");
        tooltext.setText(disease);


        logoutImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog alert = builder.create();
                alert.setTitle("Logout");
                alert.show();

            }
        });


        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(TrackListActivity.this, CategoryActivity.class);
                startActivity(intent);
                finish();

            }
        });


        tabLayout = findViewById(R.id.tabLayout);


        tabLayout.addTab(tabLayout.newTab().setText("TRACK LIST"));
        tabLayout.addTab(tabLayout.newTab().setText("PLAY LIST"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        viewPager = findViewById(R.id.pager);

        adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                viewPager.setCurrentItem(position, false);
                tabLayout.getTabAt(position).select();
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

        });

        tabLayout.setOnTabSelectedListener(this);


    }


    @Override
    public void onBackPressed() {

        Intent intent = new Intent(TrackListActivity.this, CategoryActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();

    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {

        viewPager.setCurrentItem(tab.getPosition());
        adapter.notifyDataSetChanged();

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


}
