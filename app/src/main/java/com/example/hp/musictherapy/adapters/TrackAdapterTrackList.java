package com.example.hp.musictherapy.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.hp.musictherapy.R;
import com.example.jean.jcplayer.JcAudio;
import java.util.List;

public class TrackAdapterTrackList extends RecyclerView.Adapter<TrackAdapterTrackList.MyViewHolder> {

    private List<JcAudio> trackItems;
    private static TrackAdapterTrackList.OnItemClickListener mListener;

    // Define the mListener interface
    public interface OnItemClickListener {

        void onItemClick(int position);
        void onSongItemMenuClicked(int position, View view);

    }

    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(TrackAdapterTrackList.OnItemClickListener listener) {

        this.mListener = listener;

    }

    public TrackAdapterTrackList(List<JcAudio> jcAudioList) {

        this.trackItems = jcAudioList;
        setHasStableIds(true);
        this.notifyDataSetChanged();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView trackno, trackname;
        public LinearLayout overflow;

        public MyViewHolder(View view) {
            super(view);

            trackno = view.findViewById(R.id.trackno);
            trackname = view.findViewById(R.id.trackname);
            overflow = view.findViewById(R.id.overflow_layout);


            itemView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (mListener != null) mListener.onItemClick(getAdapterPosition());

                }

            });


            overflow.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    if (mListener != null) mListener.onSongItemMenuClicked(getAdapterPosition(), overflow);

                }

            });

        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tracklist_row, parent, false);

        return new MyViewHolder(itemView);

    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        JcAudio item = trackItems.get(position);
        holder.trackno.setText("" + (position + 1));
        holder.trackname.setText(item.getTitle());

    }


    @Override
    public int getItemCount() {
        return trackItems.size();
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

}
