package com.example.hp.musictherapy.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.hp.musictherapy.R;
import com.example.hp.musictherapy.beans.DiseaseBean;
import java.util.ArrayList;

public class DiseaseAdapter extends BaseAdapter {

    private ArrayList<DiseaseBean> diseaseBeans;
    private LayoutInflater mInflater;

    public DiseaseAdapter(Context context, ArrayList<DiseaseBean> results) {

        diseaseBeans = results;
        mInflater = LayoutInflater.from(context);
        this.notifyDataSetChanged();

    }

    public int getCount() {
        return diseaseBeans.size();
    }

    public Object getItem(int position) {
        return diseaseBeans.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.adapter_disease, null);

            holder = new ViewHolder();
            holder.diseaseId = convertView.findViewById(R.id.diseaseId);
            holder.diseaseText = convertView.findViewById(R.id.diseaseText);
            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();

        }

        holder.diseaseId.setText(diseaseBeans.get(position).getId());
        holder.diseaseText.setText(diseaseBeans.get(position).getDisease());
        holder.diseaseId.setVisibility(View.GONE);

        return convertView;

    }


    class ViewHolder {

        TextView diseaseId;
        TextView diseaseText;

    }

}
