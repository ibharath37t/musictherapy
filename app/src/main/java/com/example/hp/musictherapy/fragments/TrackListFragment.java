package com.example.hp.musictherapy.fragments;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import com.example.hp.musictherapy.R;
import com.example.hp.musictherapy.adapters.TrackAdapterTrackList;
import com.example.hp.musictherapy.utils.DatabaseHelper;
import com.example.hp.musictherapy.utils.ServiceHandler;
import com.example.jean.jcplayer.JcAudio;
import com.example.jean.jcplayer.JcPlayerView;
import com.example.jean.jcplayer.JcStatus;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class TrackListFragment extends Fragment {


    JcPlayerView player;
    RecyclerView tracklist;
    ArrayList<JcAudio> jcAudios = new ArrayList<JcAudio>();
    TrackAdapterTrackList trackAdapterTrackList;
    TextView textView;
    DatabaseHelper helper;
    String track_url = "", track_response = "";
    String trackid = "", trname = "", trurl = "";
    String disease = "", genre = "";


    public TrackListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_track_list, container, false);


        player = view.findViewById(R.id.jcplayer);
        tracklist = view.findViewById(R.id.recycler_view);
        textView = view.findViewById(R.id.textView);
        trackAdapterTrackList = new TrackAdapterTrackList(jcAudios);

        track_url = getString(R.string.trackurl);
        helper = new DatabaseHelper(getActivity());

        SharedPreferences preferences = getActivity().getSharedPreferences("disease_pref", MODE_PRIVATE);
        //diseaseid = preferences.getString("disid", "");
        disease = preferences.getString("disease", "");
        SharedPreferences preferences2 = getActivity().getSharedPreferences("genre_pref", MODE_PRIVATE);
        //genid = preferences2.getString("genid", "");
        genre = preferences2.getString("genre", "");
        SharedPreferences preferences3 = getActivity().getSharedPreferences("artist_pref", MODE_PRIVATE);
        //artistid = preferences3.getString("artid", "");
        //artist = preferences3.getString("artist", "");

        new getTracksTask().execute();

        return view;

    }


    protected void adapterSetup() {


        trackAdapterTrackList = new TrackAdapterTrackList(player.getMyPlaylist());

        trackAdapterTrackList.setOnItemClickListener(new TrackAdapterTrackList.OnItemClickListener() {


            @Override
            public void onItemClick(int position) {

                player.playAudio(player.getMyPlaylist().get(position));

            }


            @Override
            public void onSongItemMenuClicked(final int position, View view) {


                PopupMenu popupMenu = new PopupMenu(getActivity(), view);
                popupMenu.getMenuInflater().inflate(R.menu.popup_menu_tracklist, popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {

                            case R.id.optPlay:

                                player.playAudio(player.getMyPlaylist().get(position));
                                return true;

                            case R.id.optPlaylist:

                                String tname = jcAudios.get(position).getTitle();
                                String turl = jcAudios.get(position).getPath();

                                if (helper.checkTrack(tname)) {

                                    Toast.makeText(getActivity(),
                                            "track already exists in playlist", Toast.LENGTH_SHORT).show();

                                } else {

                                    helper.insertTrack(position, tname, turl);
                                    Toast.makeText(getActivity(),
                                            "inserted : " + tname, Toast.LENGTH_SHORT).show();

                                }

                                return true;

                            default:
                                return false;

                        }

                    }

                });

                popupMenu.show();

            }

        });

    }


    @Override
    public void onDestroy() {

        super.onDestroy();
        try {
            if (player.isPlaying()) {
                player.kill();
            }
        } catch (Exception e) {

            return;
        }

    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            getFragmentManager().beginTransaction().detach(this).attach(this).commit();

        }

    }


    public class getTracksTask extends AsyncTask<Void, Void, Void>
            implements JcPlayerView.OnInvalidPathListener, JcPlayerView.JcPlayerViewStatusListener {

        ServiceHandler serviceHandler = new ServiceHandler();


        @Override
        protected void onPreExecute() {

            super.onPreExecute();

        }


        @Override
        protected Void doInBackground(Void... voids) {

            List<NameValuePair> valuePairs = new ArrayList<>(2);
            valuePairs.add(new BasicNameValuePair("diseaseid", disease));
            valuePairs.add(new BasicNameValuePair("genre", genre));

            try {

                track_response = serviceHandler.makeServiceCall(track_url, serviceHandler.POST, valuePairs);

            } catch (Exception e) {
                e.printStackTrace();
                track_response = "Fail";
            }

            JSONObject jsonObject = null;
            try {

                if (jcAudios.size() != 0) {
                    jcAudios.clear();
                }

                jsonObject = new JSONObject(track_response);

                for (int i = 0; i < jsonObject.getJSONArray("details").length(); i++) {

                    JSONObject jsonObject1 = jsonObject.getJSONArray("details").getJSONObject(i);

                    //trackid = jsonObject1.getString("Tracks_Id");
                    trname = jsonObject1.getString("Track_Name");
                    trurl = jsonObject1.getString("Track_URL");

                    jcAudios.add(JcAudio.createFromURL(trname, trurl));

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


            return null;
        }


        @Override
        protected void onPostExecute(Void aVoid) {


            if (jcAudios.isEmpty()) {

                tracklist.setVisibility(View.GONE);
                player.setVisibility(View.GONE);
                textView.setVisibility(View.VISIBLE);

                textView.setText("Tracks not found for " + genre + ". Please try different Genre");

            } else {

                tracklist.setVisibility(View.VISIBLE);
                player.setVisibility(View.VISIBLE);
                textView.setVisibility(View.GONE);

                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                tracklist.setLayoutManager(mLayoutManager);
                tracklist.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
                tracklist.setAdapter(trackAdapterTrackList);
                trackAdapterTrackList.notifyDataSetChanged();

                player.initPlaylist(jcAudios);
                player.playAudio(player.getMyPlaylist().get(0));
                player.registerInvalidPathListener(this);
                player.registerStatusListener(this);
                adapterSetup();

            }

            super.onPostExecute(aVoid);
        }


        @Override
        public void onPathError(JcAudio jcAudio) {

        }

        @Override
        public void onPausedStatus(JcStatus jcStatus) {

        }

        @Override
        public void onContinueAudioStatus(JcStatus jcStatus) {

        }

        @Override
        public void onPlayingStatus(JcStatus jcStatus) {

        }

        @Override
        public void onTimeChangedStatus(JcStatus jcStatus) {

        }

        @Override
        public void onCompletedAudioStatus(JcStatus jcStatus) {

        }

        @Override
        public void onPreparedAudioStatus(JcStatus jcStatus) {

        }

    }

}