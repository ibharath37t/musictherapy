package com.example.hp.musictherapy.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.hp.musictherapy.R;
import com.example.hp.musictherapy.beans.ArtistBean;
import java.util.ArrayList;

public class ArtistAdapter extends BaseAdapter {

    private ArrayList<ArtistBean> artistBeans;
    private LayoutInflater mInflater;

    public ArtistAdapter(Context context, ArrayList<ArtistBean> results) {

        artistBeans = results;
        mInflater = LayoutInflater.from(context);
        this.notifyDataSetChanged();

    }

    public int getCount() {
        return artistBeans.size();
    }

    public Object getItem(int position) {
        return artistBeans.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.adapter_artist, null);

            holder = new ViewHolder();
            holder.artistId = convertView.findViewById(R.id.artistId);
            holder.artistText = convertView.findViewById(R.id.artistText);
            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();

        }

        holder.artistId.setText(artistBeans.get(position).getId());
        holder.artistText.setText(artistBeans.get(position).getArtist());
        holder.artistId.setVisibility(View.GONE);

        return convertView;

    }


    class ViewHolder {

        TextView artistId;
        TextView artistText;

    }

}
