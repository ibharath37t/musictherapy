package com.example.hp.musictherapy.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.hp.musictherapy.R;
import com.example.hp.musictherapy.beans.GenreBean;
import java.util.ArrayList;

public class GenreAdapter extends BaseAdapter {

    private ArrayList<GenreBean> genreBeans;
    private LayoutInflater mInflater;

    public GenreAdapter(Context context, ArrayList<GenreBean> results) {

        genreBeans = results;
        mInflater = LayoutInflater.from(context);
        this.notifyDataSetChanged();

    }

    public int getCount() {
        return genreBeans.size();
    }

    public Object getItem(int position) {
        return genreBeans.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.adapter_genre, null);

            holder = new ViewHolder();
            holder.genreId = convertView.findViewById(R.id.genreId);
            holder.genreText = convertView.findViewById(R.id.genreText);
            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();

        }

        holder.genreId.setText(genreBeans.get(position).getId());
        holder.genreText.setText(genreBeans.get(position).getGenre());
        holder.genreId.setVisibility(View.GONE);

        return convertView;

    }


    class ViewHolder {

        TextView genreId;
        TextView genreText;

    }

}
