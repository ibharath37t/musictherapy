package com.example.hp.musictherapy.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import com.example.hp.musictherapy.R;
import com.example.hp.musictherapy.adapters.TrackAdapterPlayList;
import com.example.hp.musictherapy.utils.DatabaseHelper;
import com.example.jean.jcplayer.JcAudio;
import com.example.jean.jcplayer.JcPlayerView;
import com.example.jean.jcplayer.JcStatus;
import java.util.ArrayList;

public class PlayListFragment extends Fragment
        implements JcPlayerView.OnInvalidPathListener, JcPlayerView.JcPlayerViewStatusListener {


    JcPlayerView player2;
    RecyclerView playlist;
    TrackAdapterPlayList trackAdapterPlayList;
    TextView textView;
    ArrayList<JcAudio> jcAudioArrayList = new ArrayList<JcAudio>();
    ArrayList<JcAudio> jcAudios = new ArrayList<JcAudio>();
    DatabaseHelper helper;


    public PlayListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_play_list, container, false);


        player2 = view.findViewById(R.id.jcplayer);
        playlist = view.findViewById(R.id.recycler_view);
        trackAdapterPlayList = new TrackAdapterPlayList(jcAudios);
        textView = view.findViewById(R.id.textView);


        if (jcAudioArrayList.isEmpty()) {

        } else {

            jcAudioArrayList.clear();

        }

        if (jcAudios.isEmpty()) {

        } else {

            jcAudios.clear();

        }


        helper = new DatabaseHelper(getActivity());

        jcAudioArrayList = helper.getTracks();

        for (int i = 0; i < jcAudioArrayList.size(); i++) {

            //long id = jcAudioArrayList.get(i).getId();
            String title = jcAudioArrayList.get(i).getTitle();
            String turl = jcAudioArrayList.get(i).getPath();

            jcAudios.add(JcAudio.createFromURL(title, turl));

        }


        if (jcAudios.isEmpty()) {

            playlist.setVisibility(View.GONE);
            player2.setVisibility(View.GONE);
            textView.setVisibility(View.VISIBLE);

        } else {

            playlist.setVisibility(View.VISIBLE);
            player2.setVisibility(View.VISIBLE);
            textView.setVisibility(View.GONE);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            playlist.setLayoutManager(mLayoutManager);
            playlist.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            playlist.setAdapter(trackAdapterPlayList);
            trackAdapterPlayList.notifyDataSetChanged();

            player2.initPlaylist(jcAudios);
            player2.playAudio(player2.getMyPlaylist().get(0));
            player2.registerInvalidPathListener(this);
            player2.registerStatusListener(this);
            adapterSetup();

        }

        return view;

    }


    protected void adapterSetup() {


        trackAdapterPlayList = new TrackAdapterPlayList(player2.getMyPlaylist());

        trackAdapterPlayList.setOnItemClickListener(new TrackAdapterPlayList.OnItemClickListener() {


            @Override
            public void onItemClick(int position) {

                player2.playAudio(player2.getMyPlaylist().get(position));

            }


            @Override
            public void onSongItemMenuClicked(final int position, View view) {


                PopupMenu popupMenu = new PopupMenu(getActivity(), view);
                popupMenu.getMenuInflater().inflate(R.menu.popup_menu_playlist, popupMenu.getMenu());

                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {

                            case R.id.optPlay:

                                player2.playAudio(player2.getMyPlaylist().get(position));
                                return true;

                            case R.id.optPlaylist:

                                String tname = jcAudios.get(position).getTitle();

                                helper.deleteTrack(tname);
                                jcAudioArrayList.clear();
                                jcAudios.clear();

                                jcAudioArrayList = helper.getTracks();

                                for (int i = 0; i < jcAudioArrayList.size(); i++) {

                                    //long id = jcAudioArrayList.get(i).getId();
                                    String title = jcAudioArrayList.get(i).getTitle();
                                    String turl = jcAudioArrayList.get(i).getPath();

                                    jcAudios.add(JcAudio.createFromURL(title, turl));

                                }

                                if (jcAudios.isEmpty()) {

                                    textView.setVisibility(View.VISIBLE);
                                    playlist.setVisibility(View.GONE);
                                    player2.setVisibility(View.GONE);

                                    if (player2.isPlaying()) {
                                        player2.kill();
                                    }

                                } else {

                                    textView.setVisibility(View.GONE);
                                    playlist.setVisibility(View.VISIBLE);
                                    player2.setVisibility(View.VISIBLE);

                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
                                    playlist.setLayoutManager(mLayoutManager);
                                    playlist.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
                                    playlist.setAdapter(trackAdapterPlayList);
                                    trackAdapterPlayList.notifyDataSetChanged();

                                    player2.initPlaylist(jcAudios);
                                    player2.playAudio(player2.getMyPlaylist().get(0));

                                    Toast.makeText(getActivity().getApplicationContext(),
                                            "removed from playlist : " + tname, Toast.LENGTH_SHORT).show();

                                }

                                return true;

                            default:
                                return false;

                        }

                    }

                });

                popupMenu.show();

            }

        });

    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {

            getFragmentManager().beginTransaction().detach(this).attach(this).commit();

        }

    }


    @Override
    public void onDestroy() {

        super.onDestroy();
        try {
            if (player2.isPlaying()) {
                player2.kill();
            }
        } catch (Exception e) {

            return;
        }

    }


    @Override
    public void onPathError(JcAudio jcAudio) {

    }

    @Override
    public void onPausedStatus(JcStatus jcStatus) {

    }

    @Override
    public void onContinueAudioStatus(JcStatus jcStatus) {

    }

    @Override
    public void onPlayingStatus(JcStatus jcStatus) {

    }

    @Override
    public void onTimeChangedStatus(JcStatus jcStatus) {

    }

    @Override
    public void onCompletedAudioStatus(JcStatus jcStatus) {

    }

    @Override
    public void onPreparedAudioStatus(JcStatus jcStatus) {

    }
}
