package com.example.hp.musictherapy.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import com.example.hp.musictherapy.R;
import com.example.hp.musictherapy.utils.SessionManager;

public class SplashScreenActivity extends AppCompatActivity {


    private static int SPLASH_TIME_OUT = 3000;
    SessionManager sessionManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        sessionManager = new SessionManager(getApplicationContext());

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

                sessionManager.checkLogin();
                finish();

            }

        }, SPLASH_TIME_OUT);


    }


}
